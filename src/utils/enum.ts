export enum MODE  {
    ADD="ADD",
    VIEW="VIEW",
    EDIT="EDIT"
}

export enum statusPayment {
    paid="paid",
    unpaid="unpaid",
    cash="cash"
}

export enum statusInvoice {
    cancelled="cancelled",
    completed="completed",
    processing="processing",
    todo="todo"
}